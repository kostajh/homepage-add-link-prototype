# Homepage Add Link Prototype

Run with `php -S localhost:8888 router.php`

Then visit https://localhost:8888 in your browser.

Note, don't actually try to login (it won't work), instead we fake that the user is logged-in.
