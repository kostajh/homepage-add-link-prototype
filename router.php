<?php

$file = $_SERVER['SCRIPT_FILENAME'];
$reset_headers = false;
if ( $_SERVER['SCRIPT_NAME'] !== '/index.html' || $_SERVER['REQUEST_URI'] !== '/' ) {
	$file = 'https://test.m.wikipedia.org' . $_SERVER['REQUEST_URI'];
	$reset_headers = true;
}

if ( strpos( $_SERVER['REQUEST_URI'], '/newcomertask/' ) !== false ) {
	$parts = explode( '/', $_SERVER['PATH_INFO'] );
	$wikiId = $parts[4];
	$file = 'https://test.m.wikipedia.org?curid=' . $wikiId;
}

$content = file_get_contents( $file );
if ( $reset_headers ) {
	foreach( $http_response_header as $header ) {
		header( $header );
	}
}
if ( $_SERVER['SCRIPT_NAME'] === '/w/api.php' && $_POST && $_POST['action'] === 'options' ) {
	header('application/json; charset=utf-8' );
	echo json_encode( [ 'options' => 'success' ] );
	return true;
}

echo $content;
return true;
